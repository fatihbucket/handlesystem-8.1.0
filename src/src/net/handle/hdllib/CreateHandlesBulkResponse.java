/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.

        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
          http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.handle.hdllib;

public class CreateHandlesBulkResponse extends AbstractResponse {

	
	public CreateHandlesBulkResponse() {
	    super(OC_CREATE_HANDLES_BULK, AbstractMessage.RC_SUCCESS);
	  
	}
	
	public CreateHandlesBulkResponse(AbstractRequest req) throws HandleException {
	    super(req, AbstractMessage.RC_SUCCESS);
	
	}
}
