/*
 * Author:Fatih Berber
 *
 * */

package net.handle.hdllib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SingleHandleRecord {
	
	public byte [] handle;
	public HandleValue [] values;
	
	public boolean exists = false;
	public byte action;
   
    public byte[][] existingRawValues;
    public boolean authorized;
    public long date = 0;
    
   
    public SingleHandleRecord(byte [] handle, HandleValue [] values){
    	this.handle = handle;
    	this.values = values;
    }
    
    
    public byte [] getRecordBytes(){
    	// add the size of all of the clumps as well as a 'time-received' field
    	// and 'value-length' field for each value
    	int totalSize = Encoder.INT_SIZE;
    	for( int i=0; i < values.length; i++) {
    		totalSize += ( Encoder.calcStorageSize(values[i]) + Encoder.INT_SIZE);
    	}
    
        // write all of the clumps to a buffer
       byte data[] = new byte[totalSize];
       int offst = 0;
       offst += Encoder.writeInt( data, offst, values.length);
       for( int i=0; i < values.length; i++) {
          int clumpLen = Encoder.encodeHandleValue( data, offst+Encoder.INT_SIZE,
                                                values[i]);
          offst += Encoder.writeInt(data, offst, clumpLen);
          offst += clumpLen;
        }
       return data;
    }
}
