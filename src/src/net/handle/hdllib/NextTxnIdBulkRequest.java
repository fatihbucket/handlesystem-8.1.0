/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.

        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
          http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.handle.hdllib;

public class NextTxnIdBulkRequest 
  extends AbstractRequest
  {
  public int bulkSize;
  
  public NextTxnIdBulkRequest(byte handle[]) 
  {
    super(handle, OC_GET_NEXT_BULK_TXN_ID, null);
  }  
  
  public NextTxnIdBulkRequest(byte handle[], int bulkSize) 
  {
    super(handle, OC_GET_NEXT_BULK_TXN_ID, null);
    this.bulkSize = bulkSize;
  
  } 
}


