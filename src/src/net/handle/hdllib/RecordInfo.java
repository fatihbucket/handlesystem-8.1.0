/*
 * Author:Fatih Berber
 *
 * */

package net.handle.hdllib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecordInfo {
		
	public ArrayList<int[]> typeDataPairs = new ArrayList<int[]>();
	public String suffix;
	
	public RecordInfo() {};
	
    public RecordInfo(String suffix) {
        this.suffix = suffix;
    }
    
    public void addTypeDataPair(int typeIdx, int dataIdx){
    	int [] typeDataPair ={typeIdx,dataIdx};
    	typeDataPairs.add(typeDataPair);
    }
    
    public int[] getTypeDataPair(int pairIdx){
    	return typeDataPairs.get(pairIdx);
    }
    public int size(){
    	return typeDataPairs.size();
    	
    }
    
}