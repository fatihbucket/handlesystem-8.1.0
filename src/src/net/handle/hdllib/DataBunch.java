/*
 * Author:Fatih Berber
 *
 * */

package net.handle.hdllib;

import java.util.List;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;

public class DataBunch {

	private Map<Integer, byte[]> dataStorage = new HashMap<Integer, byte[]>();
	
	public byte [] getData(int hashCode){
		return dataStorage.get(hashCode);
	}
	public int addData(byte [] data){
	  int dataHashCode =  java.util.Arrays.hashCode(data);
	  if (!dataStorage.containsKey(dataHashCode)){
		  dataStorage.put(dataHashCode,data);
	  }
	  return dataHashCode;
	}
	
	public int size(){
		return dataStorage.size();
	}
	
	public Set<Integer> keySet(){
		return dataStorage.keySet();
	}	
   
}