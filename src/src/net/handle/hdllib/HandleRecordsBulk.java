/*
 * Author:Fatih Berber
 *
 * */

package net.handle.hdllib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

public class HandleRecordsBulk {
	
	private ArrayList<RecordInfo> recordsInfoList = new ArrayList<RecordInfo>();
	private Map<Integer, TypeRecord> typeRecordBunch = new HashMap<Integer, TypeRecord>();
	private Map<Integer, SingleHandleRecord> singleHandleRecordsCache = new HashMap<Integer,SingleHandleRecord>();
	
    public String prefix;
    public byte handle[];

    public HandleRecordsBulk(String prefix) {
    	this.prefix = prefix;     
    }
 
    /*for decoder only*/
    public void addTypeRecord(TypeRecord typeRecord){
    	typeRecordBunch.put(typeRecord.hashCode(),typeRecord);
    }
    
    /*for decoder only*/
    public void addRecordInfo(RecordInfo recordInfo){
    	if (recordsInfoList.size() == 0){
    		String suffix = recordInfo.suffix;
			this.handle = Util.assemblePrefixSuffix(Util.encodeString(this.prefix),Util.encodeString(suffix));
		}
    	recordsInfoList.add(recordInfo);
    	
    }
    
    public void add(String suffix, HandleValue [] values){
   	
    	if(values.length > 0){
    		
    		if (recordsInfoList.size() == 0){
    			this.handle = Util.assemblePrefixSuffix(Util.encodeString(this.prefix),Util.encodeString(suffix));
    		}
    		
    		RecordInfo recordInfo = new RecordInfo(suffix);
    		recordsInfoList.add(recordInfo); 
    		for(int i=0;i<values.length;i++){
    			
    			int hashCode = values[i].hashCodeForBulk();
    			TypeRecord typeRecord = this.typeRecordBunch.get(hashCode);
    			if(typeRecord == null){
    				typeRecord = new TypeRecord(values[i]);
    				typeRecordBunch.put(hashCode,typeRecord);
    			}
    			int dataIdx = typeRecord.addData(values[i].getData().clone());
    			recordInfo.addTypeDataPair(hashCode,dataIdx);			
    		}
    		
    	}
    }
     
    private HandleValue [] getValues(int recordIdx){
    	
    	RecordInfo recordInfo = recordsInfoList.get(recordIdx);
    	HandleValue [] values = new HandleValue[recordInfo.typeDataPairs.size()];
    	for(int i=0;i<recordInfo.typeDataPairs.size();i++){
    		int[] typeDataPair = recordInfo.getTypeDataPair(i);
    		int hashCode = typeDataPair[0];
    		int dataIdx = typeDataPair[1];
    		TypeRecord typeRecord = typeRecordBunch.get(hashCode);
    		HandleValue value = new HandleValue();
    		values[i] = value;
    		typeRecord.getHandleValue(dataIdx,value);
    		
    	}
    	return values;
    	
    }
    
    public int size(){
    	return recordsInfoList.size();
    }
    
    public TypeRecord getTypeRecord(int hashCode){
    	return typeRecordBunch.get(hashCode);
    }
	
    
    public int  getTypeBunchSize(){
    	return typeRecordBunch.size();
    }
    
    public Set<Integer> getTypeBunchKeys(){
    	return typeRecordBunch.keySet();
    }

	public RecordInfo getRecordInfo(int recordIdx){
		return this.recordsInfoList.get(recordIdx);
	}
    
   public SingleHandleRecord getHandleRecord(int recordIdx){
	  
	   SingleHandleRecord handleRecord = singleHandleRecordsCache.get(recordIdx);
	   if (handleRecord == null){
		   RecordInfo recordInfo  = recordsInfoList.get(recordIdx); 
	   	   String suffix = recordInfo.suffix;
	       byte [] handle =  Util.assemblePrefixSuffix(Util.encodeString(this.prefix),Util.encodeString(suffix));
	   	   HandleValue [] values = getValues(recordIdx);
	   	   handleRecord = new SingleHandleRecord(handle,values);
	   	   singleHandleRecordsCache.put(recordIdx,handleRecord);
	   }
	   
   	   return handleRecord;
   	 }
       
    
}
