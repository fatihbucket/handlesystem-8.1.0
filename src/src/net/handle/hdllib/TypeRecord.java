/*
 * Author:Fatih Berber
 *
 * */

package net.handle.hdllib;

import java.util.ArrayList;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class TypeRecord {
	
	/* HandleValue fields without data field*/
	int index = -1;
	byte[] type = Common.EMPTY_BYTE_ARRAY;
	byte ttlType = HandleValue.TTL_TYPE_RELATIVE;
	int ttl = 86400;
	int timestamp = 0;
	ValueReference[] references = null;
	boolean adminRead = true;     // indicates whether or not admins can read this value
	boolean adminWrite = true;    // indicates whether or not admins can modify this value
	boolean publicRead = true;    // indicates whether or not anyone can read this value
	boolean publicWrite = false;   // indicates whether or not anyone can modify this value

	
	public DataBunch dataBunch = new DataBunch();
	
	public TypeRecord(){};
	
	public TypeRecord(HandleValue value){
		
		this.index = value.index;
		this.type = value.type.clone();
		this.ttlType = value.ttlType;
		this.ttl = value.ttl;
		this.timestamp = value.timestamp;
		this.adminRead = value.adminRead;
		this.adminWrite = value.adminWrite;
		this.publicRead = value.publicRead;
		this.publicWrite = value.publicWrite;
		if(value.references != null)
			this.references = value.references.clone();
		
	}
	
	public int addData(byte [] data){
		return dataBunch.addData(data);
	}
	
	public byte [] getType(){
		return type;
		
	}

	public void getHandleValue(int dataItemIdx, HandleValue value){
		
		value.index = this.index;
		value.type = this.type;
		value.ttlType = this.ttlType;
		value.ttl = this.ttl;
		value.timestamp = this.timestamp;
		value.adminRead = this.adminRead;
		value.adminWrite = this.adminWrite;
		value.publicRead = this.publicRead;
		value.publicWrite = this.publicWrite;
		value.references = this.references;
		value.data = this.dataBunch.getData(dataItemIdx);
		
	}
	
	 public int hashCode() {
	      final int prime = 31;
	      int result = 1;
	      result = prime * result + (adminRead ? 1231 : 1237);
	      result = prime * result + (adminWrite ? 1231 : 1237);
	      result = prime * result + index;
	      result = prime * result + (publicRead ? 1231 : 1237);
	      result = prime * result + (publicWrite ? 1231 : 1237);
	      result = prime * result + Arrays.hashCode(emptyToNull(references));
	      result = prime * result + timestamp;
	      result = prime * result + ttl;
	      result = prime * result + ttlType;
	      result = prime * result + Arrays.hashCode(type);
	      return result;
	  }
	
	 
	
	 private static <T> T[] emptyToNull(T[] array) {
	      if (array != null && array.length == 0) return null;
	      return array;
	  }

	  
   
}