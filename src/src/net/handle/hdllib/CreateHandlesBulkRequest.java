/**********************************************************************\
 © COPYRIGHT 2015 Corporation for National Research Initiatives (CNRI);
                        All rights reserved.

        The HANDLE.NET software is made available subject to the
      Handle.Net Public License Agreement, which may be obtained at
          http://hdl.handle.net/20.1000/103 or hdl:20.1000/103
\**********************************************************************/

package net.handle.hdllib;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set; 


/******************************************************************************
 * Request used to create a new handle.  Holds the handle and the
 * initial values.
 ******************************************************************************/

public class CreateHandlesBulkRequest extends AbstractRequest {
	public HandleRecordsBulk recordsBulk;
	
	
	public CreateHandlesBulkRequest(HandleRecordsBulk recordsBulk, AuthenticationInfo authInfo) throws HandleException {
		super(recordsBulk.handle, AbstractMessage.OC_CREATE_HANDLES_BULK, authInfo);
		
		this.recordsBulk = recordsBulk;
		this.isAdminRequest = true;

	}
	
	public CreateHandlesBulkRequest(HandleRecordsBulk recordsBulk, AuthenticationInfo authInfo, boolean mintNewSuffix)  throws HandleException {
	super(recordsBulk.handle, AbstractMessage.OC_CREATE_HANDLES_BULK, authInfo);
		
		this.recordsBulk = recordsBulk;
		this.isAdminRequest = true;
		this.mintNewSuffix = true;
	}
	
	
	@Override
	public boolean shouldEncrypt() {
		
		return false;
	}
}
